package com.apps.sirenity.prankapp;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.Display;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import pl.droidsonroids.gif.GifImageView;

public class MainActivity extends AppCompatActivity {

    HorizontalScrollView gifsLayout;
    TextView time;
    Button startButton;
    ImageButton playSoundButton;
    Spinner spinner;
    RadioButton radioPickTime;
    RadioButton radioSetTimer;
    EditText timer;

    int selectedGifDrawableId;

    AlarmManager alarmManager;
    private PendingIntent pending_intent;
    private CountDownTimer countdownTimer;
    static final String TEMP_ALARM = "temp_alarm_time";
    static final String GIF = "gif";
    static final String SOUND = "sound";
    private static final int DEFAULT_SECONDS_TO_ALARM = 60;
    Map<String, Integer> sounds = new HashMap<>();

    static{
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkExtras();
        long alarmTime = getAlarmTime();

        findAllById();
        setFonts();
        setGifSelector();
        initSpinnerAndPlayButton();
        onRadioSelect();
        configTimer();


        if (alarmTime != 0 && alarmTime > Calendar.getInstance().getTimeInMillis()) {
            setTimerCounter(alarmTime);
            setButtonStop();
        } else {
            saveAlarmTime(0);
            setTimerOnClickListener();
            setStartOnClickListener();
        }
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        final Intent myIntent = new Intent(MainActivity.this, AlarmReceiver.class);
        pending_intent = PendingIntent.getBroadcast(MainActivity.this, 0, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

    }

    private void configTimer() {
        timer.setHint("Seconds to go");
        timer.setOnClickListener((e) -> timer.setSelection(timer.getText().length()));
        timer.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if(imm!=null){
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                return true;
            }
            return false;
        });
    }

    private MediaPlayer mediaPlayer = null;
    private AtomicBoolean played = new AtomicBoolean(false);

    private void initSpinnerAndPlayButton() {
        String[] strings = {
                "Screamer 1",
                "Screamer 2",
                "Screamer 3",
                "Screamer 4",
                "Screamer 5",
                "John Cena",
                "Evil Laugh",
                "FNAF",
                "MLG",
                "Extra 1",
                "Extra 2",
                "Extra 3",
                "Extra 4"
        };

        sounds.put(strings[0], R.raw.s1);
        sounds.put(strings[1], R.raw.s2);
        sounds.put(strings[2], R.raw.s3);
        sounds.put(strings[3], R.raw.s4);
        sounds.put(strings[4], R.raw.s5);
        sounds.put(strings[5], R.raw.john_cena);
        sounds.put(strings[6], R.raw.evil_laugh);
        sounds.put(strings[7], R.raw.fnaf);
        sounds.put(strings[8], R.raw.mlg);
        sounds.put(strings[9], R.raw.hahaha_ya_poshutil);
        sounds.put(strings[10], R.raw.hotite_screemer_a_ego_ne_budet);
        sounds.put(strings[11], R.raw.mama_prishla);
        sounds.put(strings[12], R.raw.zelenii_slonik);


        ArrayAdapter<String> adp = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, strings);


        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        spinner.setAdapter(adp);
        spinner.setSelection(5);

        playSoundButton.setOnClickListener((e) -> {
            String selectedItem = (String) spinner.getSelectedItem();
            Integer soundResId = sounds.get(selectedItem);

            Drawable stopDrawable = getResources().getDrawable(R.drawable.ic_pause_black_36dp);
            Drawable playDrawable = getResources().getDrawable(R.drawable.ic_play_arrow_black_36dp);
            if (played.get()) {
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    mediaPlayer = null;
                }
                playSoundButton.setImageDrawable(playDrawable);
                played.set(false);
            } else {
                played.set(true);
                playSoundButton.setImageDrawable(stopDrawable);
                mediaPlayer = MediaPlayer.create(this, soundResId);
                mediaPlayer.setOnCompletionListener((q) -> {
                    playSoundButton.setImageDrawable(playDrawable);
                    played.set(false);
                });
                mediaPlayer.start();
            }
        });
    }

    private void onRadioSelect() {
        radioPickTime.setOnClickListener((e) -> {
            time.setVisibility(View.VISIBLE);
            timer.setVisibility(View.GONE);
        });
        radioSetTimer.setOnClickListener((e) -> {
            time.setVisibility(View.GONE);
            timer.setVisibility(View.VISIBLE);
        });
    }

    private void setTimerCounter(long alarmTime) {
        countdownTimer = new CountDownTimer(alarmTime - Calendar.getInstance().getTimeInMillis(), 1) {
            @SuppressLint("DefaultLocale")
            @Override
            public void onTick(long leftTimeInMilliseconds) {
                long seconds = leftTimeInMilliseconds / 1000;
                long hours = seconds / (60 * 60);
                long minutes = (seconds / 60) % 60;
                long secundes = seconds % 60;

                if (hours != 0) {
                    time.setText(
                            String.format("%02d:%02d:%02d", hours, minutes, secundes)
                    );
                } else if (minutes != 0) {
                    time.setText(
                            String.format("%02d:%02d", minutes, secundes)
                    );
                } else {
                    time.setText(
                            String.format("%02d", secundes)
                    );
                }

            }

            @Override
            public void onFinish() {
                setTimerOnClickListener();
                setStartOnClickListener();
            }
        }.start();

        time.setOnClickListener((e) -> startTimerActivity(alarmTime, Calendar.getInstance().getTimeInMillis()));
    }

    private void setButtonStop() {
        startButton.setText(R.string.stop);
        startButton.setOnClickListener((e) -> {
            saveAlarmTime(0);
            countdownTimer.cancel();
            stopAlarm();
            setTimerOnClickListener();
            setStartOnClickListener();
        });
    }

    private void checkExtras() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.getLong(TEMP_ALARM, -1) == 0) {
                saveAlarmTime(0);
            }
        }
    }

    private void findAllById() {
        time = findViewById(R.id.time);
        gifsLayout = findViewById(R.id.gifs);
        startButton = findViewById(R.id.start);
        spinner = findViewById(R.id.spinner);
        playSoundButton = findViewById(R.id.playSound);
        radioPickTime = findViewById(R.id.pickTime);
        radioSetTimer = findViewById(R.id.setTimer);
        timer = findViewById(R.id.timer);
    }

    private void setStartOnClickListener() {
        startButton.setText(R.string.lets_go);
        startButton.setOnClickListener((e) -> {

            final Intent myIntent = new Intent(MainActivity.this, AlarmReceiver.class);
            myIntent.putExtra(GIF, selectedGifDrawableId);
            myIntent.putExtra(SOUND, sounds.get(spinner.getSelectedItem()));

            pending_intent = PendingIntent.getBroadcast(MainActivity.this, 0, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            Calendar calendar = Calendar.getInstance();
            long currentTime = calendar.getTimeInMillis();
            long alarmTime;
            if (radioPickTime.isChecked()) {
                String[] split = time.getText().toString().split(":");
                calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(split[0]));
                calendar.set(Calendar.MINUTE, Integer.parseInt(split[1]));
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                alarmTime = calendar.getTimeInMillis();
            } else {
                if (timer.getText().toString().isEmpty()) {
                    alarmTime = currentTime + DEFAULT_SECONDS_TO_ALARM * 1000;
                } else {
                    int seconds = Integer.parseInt(timer.getText().toString());
                    alarmTime = currentTime + seconds * 1000;
                }
            }

            if (alarmTime < currentTime) {
                alarmTime += 86400000; //1 day in millis
            }
            saveAlarmTime(alarmTime);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, pending_intent);
            } else {
                alarmManager.set(AlarmManager.RTC_WAKEUP, alarmTime, pending_intent);
            }
            startTimerActivity(alarmTime, currentTime);
        });
    }

    private void startTimerActivity(long alarmTime, long currentTime) {
        final Intent timerIntent = new Intent(this, TimerActivity.class);
        timerIntent.putExtra("time", alarmTime - currentTime);

        startActivity(timerIntent);
    }

    //You can use smoothScrollTo(int x, int y) method, (x, y) represents the position of selected view, you can calculate it easily: (View.getLeft() + View.getPaddingLeft(), View.getTop()).

    private void setGifSelector() {
        Point screen = getScreenDimensions();
        int childCount = gifsLayout.getChildCount();
        if (childCount == 1) {
            LinearLayout linearLayout = (LinearLayout) gifsLayout.getChildAt(0);
            childCount = linearLayout.getChildCount();
            if (childCount > 0) {
                List<View> children = new ArrayList<>();
                for (int i = 0; i < childCount; i++) {
                    children.add(linearLayout.getChildAt(i));
                }
                for (View chView : children) {
                    chView.setOnClickListener((e) -> {
                        e.getBackground().setAlpha(0xB3);
                        ((GifImageView) e).setImageResource(R.drawable.ic_check_circle_black_24dp);
                        ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#F11c0403"), PorterDuff.Mode.ADD);
                        e.getBackground().setColorFilter(filter);
                        gifsLayout.smoothScrollTo(e.getLeft() - screen.x / 2 + (e.getRight() - e.getLeft()) / 2, e.getTop());
                        selectedGifDrawableId = parseTag(chView.getTag());
                        for (View view : children) {
                            if (view != chView) {
                                view.getBackground().setAlpha(0xFF);
                                view.getBackground().clearColorFilter();
                                ((GifImageView) view).setImageDrawable(null);
                            }
                        }
                    });
                }
            }
        }
    }

    private Point getScreenDimensions() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    private int parseTag(Object tag) {
        String tagS = (String) tag;
        String[] split = tagS.split(File.separator);
        String name = split[split.length - 1];
        name = name.split("\\.")[0];
        return this.getResources().getIdentifier(name, "drawable", this.getPackageName());
    }

    private void setTimerOnClickListener() {
        time.setText(R.string.time);
        time.setOnClickListener(v -> {
            Calendar currentTime = Calendar.getInstance();
            int hour = currentTime.get(Calendar.HOUR_OF_DAY);
            int minute = currentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker;
            mTimePicker = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    time.setText(String.format("%02d:%02d", selectedHour, selectedMinute));
                }
            }, hour, minute, true);
            mTimePicker.setTitle("Select Time");
            mTimePicker.show();

        });
    }

    private void setFonts() {
        TextView tx1 = (TextView)findViewById(R.id.choseGif);
        TextView tx2 = (TextView)findViewById(R.id.choseSound);


        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/28 Days Later.ttf");

        tx1.setTypeface(custom_font);
        tx2.setTypeface(custom_font);
        radioPickTime.setTypeface(custom_font);
        radioSetTimer.setTypeface(custom_font);
    }

    private void saveAlarmTime(long alarmTime) {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putLong(TEMP_ALARM, alarmTime);
        editor.apply();
    }

    private long getAlarmTime() {
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        return sharedPref.getLong(TEMP_ALARM, 0);
    }

    private void stopAlarm() {
        alarmManager.cancel(pending_intent);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
        finish();
        System.exit(0);
    }

    public void onDestroy() {
        super.onDestroy();
        System.exit(0);
    }
}
