package com.apps.sirenity.prankapp;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import static com.apps.sirenity.prankapp.MainActivity.TEMP_ALARM;

public class TimerActivity extends AppCompatActivity {

    ProgressBar mProgressBar1;

    private TextView textViewShowTime;
    private CountDownTimer countDownTimer;
    private FloatingActionButton stopButton;
    private long totalTimeCountInMilliseconds;

    AdView mAdView;
    AdView mAdView1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);

        long times = getTime();
        initUI();

        setTimer(times);
        startTimer();

        stopButton.setOnClickListener((e) -> {
            stopAlarm();
            countDownTimer.cancel();
            countDownTimer.onFinish();
        });


        MobileAds.initialize(getApplicationContext(),
                "ca-app-pub-4119114383258137~4998778800");

        mAdView = findViewById(R.id.adView);
        mAdView1 = findViewById(R.id.adView1);
        mAdView.loadAd(new AdRequest.Builder().build());
        mAdView1.loadAd(new AdRequest.Builder().build());
    }

    private void stopAlarm() {
        final Intent myIntent = new Intent(this, AlarmReceiver.class);
        PendingIntent pending_intent = PendingIntent.getBroadcast(TimerActivity.this, 0, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager systemService = (AlarmManager) getSystemService(ALARM_SERVICE);
        if (systemService != null) {
            systemService.cancel(pending_intent);
        }
        final Intent mainActivity = new Intent(this, MainActivity.class);
        mainActivity.putExtra(TEMP_ALARM, Long.valueOf(0));
        startActivity(mainActivity);
    }

    private void initUI() {
        mProgressBar1 = findViewById(R.id.progressbar1_timerview);
        stopButton = findViewById(R.id.fab);
        textViewShowTime = findViewById(R.id.textView_timerview_time);
    }

    private long getTime() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            return extras.getLong("time");
        }
        return 0;
    }

    private void setTimer(long times) {
        totalTimeCountInMilliseconds = times;
        mProgressBar1.setMax((int) times);
    }

    private void startTimer() {
        countDownTimer = new CountDownTimer(totalTimeCountInMilliseconds, 1) {
            @SuppressLint("DefaultLocale")
            @Override
            public void onTick(long leftTimeInMilliseconds) {
                long seconds = leftTimeInMilliseconds / 1000;
                mProgressBar1.setProgress((int) (leftTimeInMilliseconds));
                long hours = seconds / (60 * 60);
                long minutes = (seconds / 60) % 60;
                long secundes = seconds % 60;

                if (hours != 0) {
                    textViewShowTime.setText(
                            String.format("%02d:%02d:%02d", hours, minutes, secundes)
                    );
                } else if (minutes != 0) {
                    textViewShowTime.setText(
                            String.format("%02d:%02d", minutes, secundes)
                    );
                } else {
                    textViewShowTime.setText(
                            String.format("%02d", secundes)
                    );
                }

            }

            @Override
            public void onFinish() {
                textViewShowTime.setText(R.string._00_00);
                textViewShowTime.setVisibility(View.VISIBLE);
            }
        }.start();
    }

    @Override
    public void onBackPressed() {
        final Intent mainActivity = new Intent(this, MainActivity.class);
        startActivity(mainActivity);
    }

}
