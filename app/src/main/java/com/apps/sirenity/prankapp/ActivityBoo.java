package com.apps.sirenity.prankapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import pl.droidsonroids.gif.GifImageView;

import static com.apps.sirenity.prankapp.MainActivity.GIF;
import static com.apps.sirenity.prankapp.MainActivity.SOUND;

public class ActivityBoo extends AppCompatActivity {

    GifImageView viewById;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boo);
        viewById = this.findViewById(R.id.boo_image);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Integer gif = extras.getInt(GIF);
            if (gif != 0) {
                int gifDrawableId = gif;
                viewById.setImageResource(gifDrawableId);
            } else {
                viewById.setImageResource(R.drawable.g1);
            }
            int sound = extras.getInt(SOUND);
            if(sound!=0){
                MediaPlayer mediaPlayer = MediaPlayer.create(this, sound);
                mediaPlayer.start();
            }else {
                MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.john_cena);
                mediaPlayer.start();
            }
        }
    }

    @Override
    public void onBackPressed() {
        final Intent mainActivity = new Intent(this, MainActivity.class);
        startActivity(mainActivity);
    }
}
