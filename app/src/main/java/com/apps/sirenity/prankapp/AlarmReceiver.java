package com.apps.sirenity.prankapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import static com.apps.sirenity.prankapp.MainActivity.GIF;
import static com.apps.sirenity.prankapp.MainActivity.SOUND;

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent booIntent = new Intent(context, ActivityBoo.class);
        if (intent.getExtras() != null) {
            int gifId = intent.getExtras().getInt(GIF);
            int sound = intent.getExtras().getInt(SOUND);
            booIntent.putExtra(GIF, gifId);
            booIntent.putExtra(SOUND, sound);
        }
        booIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(booIntent);
    }
}